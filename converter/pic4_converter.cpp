//
// Created by thejackimonster on 19.11.17.
//

#include <opencv2/core.hpp>
#include <opencv2/imgproc.hpp>
#include <opencv2/highgui.hpp>

#include <pic4.hpp>

void decode(pic4::Image& img) {
	size_t N = pic4_calcBufferSizeByLevel(img.level);
	float fx, fy;
	uint8_t l;
	size_t j;

	for (size_t i = N - 1; i > 0; i--) {
		l = pic4_calcLevelByBufferSize(i);
		pic4_calcPositionByIndex(i, &fx, &fy);
		j = pic4_calcIndexByPosition(l, pic4_calcPrecision(l), fx, fy);
		img.buffer[i] -= img.buffer[j];
	}
}

int main(int argc, char** argv) {
	if (argc <= 1) {
		return 1;
	}

	char* path = argv[1];
	size_t pathLen = strlen(path);

	cv::Mat image = cv::imread(path, cv::ImreadModes::IMREAD_COLOR);

	if (!image.data) {
		return 2;
	}

	auto* outPath = new char[ pathLen + 6 ];

	strncpy(outPath, path, pathLen + 1);

	char* dot = strrchr(outPath, '.');

	if (dot == nullptr) {
		dot = outPath + pathLen;
	}

	strncpy(dot + 2, ".p4\0", 4);

	uint8_t level [3];

	if (argc > 2) {
		level[0] = static_cast<uint8_t>(atoi(argv[2]));
	} else {
		level[0] = pic4_calcLevelByBufferSize(
				static_cast<size_t>(image.rows * image.cols)
		);

		if (pic4_calcBufferSizeByLevel(level[0]) < (image.rows * image.cols)) {
			level[0]++;
		}
	}

	if (argc > 3) {
		level[1] = static_cast<uint8_t>(atoi(argv[3]));
	} else {
		level[1] = level[0];
	}

	if (argc > 4) {
		level[2] = static_cast<uint8_t>(atoi(argv[4]));
	} else {
		level[2] = level[0];
	}

	pic4::Image imgY (level[0]);
	pic4::Image imgU (level[1]);
	pic4::Image imgV (level[2]);

	for (pic4::Point& p : imgY) {
		auto px = image.at<cv::Vec3b>(
				cv::Point(
						static_cast<int>(p.x() * image.cols),
						static_cast<int>(p.y() * image.rows)
				)
		);

		float y = (0.299f * px[0] + 0.587f * px[1] + 0.114f * px[2]);
		float u = (0.5f * px[2] - 0.168736f * px[0] - 0.331264f * px[1]);
		float v = (0.5f * px[0] - 0.418688f * px[1] - 0.081312f * px[2]);

		auto Y = static_cast<uint8_t>(MIN(y, 0xFF));
		auto U = static_cast<uint8_t>(MIN(u + 128, 0xFF));
		auto V = static_cast<uint8_t>(MIN(v + 128, 0xFF));

		p.set(Y);

		imgU.set(p.x(), p.y(), U);
		imgV.set(p.x(), p.y(), V);
	}

	decode(imgY);
	decode(imgU);
	decode(imgV);

	strncpy(dot, "_Y", 2);
	imgY.write(outPath);

	strncpy(dot, "_U", 2);
	imgU.write(outPath);

	strncpy(dot, "_V", 2);
	imgV.write(outPath);

	delete[] outPath;

	return 0;
}
