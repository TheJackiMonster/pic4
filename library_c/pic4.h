#pragma once

//
// Created by thejackimonster on 19.11.17.
//

#ifndef PIC4_H_
#define PIC4_H_

#include <stddef.h>
#include <stdint.h>

size_t pic4_calcBufferSizeByLevel(uint8_t level);

float pic4_calcPrecision(uint8_t level);

uint8_t pic4_calcLevelByBufferSize(size_t size);

void pic4_calcPositionByIndex(size_t index, float* x, float* y);

float pic4_calcDistance(float dx, float dy);

size_t pic4_calcIndexByPosition(uint8_t level, float precision, float x, float y);

typedef struct pic4_Image {
	uint8_t level;
	float precision;
	uint8_t* buffer;
} pic4_Image;

void pic4_Image_create(pic4_Image* image, uint8_t level);

void pic4_Image_destroy(pic4_Image* image);

uint8_t pic4_Image_get(const pic4_Image* image, float x, float y);

void pic4_Image_set(pic4_Image* image, float x, float y, uint8_t value);

void pic4_Image_read(pic4_Image* image, const char* filename);

void pic4_Image_write(const pic4_Image* image, const char* filename);

#endif /* PIC4_H_ */