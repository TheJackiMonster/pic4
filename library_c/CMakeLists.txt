cmake_minimum_required(VERSION 3.9)
project(PIC4_LIB_C C)

set(CMAKE_C_STANDARD 11)

find_package(OpenMP QUIET)

if (OpenMP_FOUND)
	set(CMAKE_C_FLAGS ${CMAKE_C_FLAGS} ${OpenMP_C_FLAGS})
endif()

add_library(pic4 SHARED pic4.h pic4.c)

target_include_directories(pic4 PUBLIC SYSTEM ${CMAKE_CURRENT_SOURCE_DIR})
