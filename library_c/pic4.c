//
// Created by thejackimonster on 19.11.17.
//

#include "pic4.h"

#include <stdio.h>
#include <stdlib.h>

#define ABS(x) (x < 0? -x : x)

size_t pic4_calcBufferSizeByLevel(uint8_t level) {
	size_t bufferSize = 0;

#pragma omp parallel for simd reduction(+:bufferSize)
	for (int i = 0; i < level; i++) {
		bufferSize += (1 << (2 * i));
	}

	return bufferSize;
}

float pic4_calcPrecision(uint8_t level) {
	return 2.0f / (1 << (level));
}

uint8_t pic4_calcLevelByBufferSize(size_t bufferSize) {
	uint8_t level = 0;
	size_t size = 1;

	while (bufferSize >= size) {
		bufferSize -= size;
		size <<= 2;
		level++;
	}

	return level; // (uint8_t) (log_2(bufferSize) + 1);
}

void pic4_calcPositionByIndex(size_t index, float* x, float* y) {
	size_t size = 1;
	size_t width = 1;

	while (index >= size) {
		index -= size;
		size <<= 2;
		width <<= 1;
	}

	size_t px = index % width;
	size_t py = index / width;

	*x = (0.5f + px) / width;
	*y = (0.5f + py) / width;
}

float pic4_calcDistance(float dx, float dy) {
	return ABS(dx) + ABS(dy);
}

size_t pic4_calcIndexByPosition(uint8_t level, float precision, float x, float y) {
	size_t width = 1, offset = 0;

	float fx = (x - 0.5f), fy = (y - 0.5f);

	float c = 0.25f;

	const uint8_t max_level = level;

	uint8_t hit_level = 0;
	size_t hit_offset = 0;

	while (--level > 0) {
		if (pic4_calcDistance(fx, fy) < precision) {
			hit_level = max_level - level;
			hit_offset = offset;
			break;
		}

		if (fx >= 0) {
			fx -= c;
		} else {
			fx += c;
		}

		if (fy >= 0) {
			fy -= c;
		} else {
			fy += c;
		}

		offset += width * width;
		width <<= 1;

		c *= 0.5f;
	}

	if (hit_level > 0) {
		offset = hit_offset;
		width = (size_t) (1L << (hit_level - 1L));
	}

	size_t px = (size_t) (x * width);
	size_t py = (size_t) (y * width);

	if (px < 0) {
		px = 0;
	} else
	if (px >= width) {
		px = width - 1;
	}

	if (py < 0) {
		py = 0;
	} else
	if (py >= width) {
		py = width - 1;
	}

	return offset + px + py * width;
}

void pic4_Image_create(pic4_Image* image, uint8_t level) {
	image->level = level;
	image->precision = pic4_calcPrecision(level);
	image->buffer = (uint8_t*) malloc(sizeof(uint8_t) * pic4_calcBufferSizeByLevel(level));
}

void pic4_Image_destroy(pic4_Image* image) {
	free((void*) image->buffer);
}

uint8_t pic4_Image_get(const pic4_Image* image, float x, float y) {
	const size_t best = pic4_calcIndexByPosition(image->level, image->precision, x, y);
	const uint8_t level = (uint8_t) (0x1 + pic4_calcLevelByBufferSize(best));
	
	const uint8_t value = *(image->buffer + best);
	
	if (level < image->level) {
		float fx, fy;

		pic4_calcPositionByIndex(best, &fx, &fy);

		const float f = pic4_calcDistance((fx - x), (fy - y)) / image->precision;
		
		if ((f >= 0.5f) && (f <= 1.0f)) {
			const size_t next = pic4_calcIndexByPosition(image->level, image->precision * 0.5f, x, y);
			
			return (uint8_t) (value * (2.0f - f * 2.0f) + *(image->buffer + next) * (f * 2.0f - 1.0f));
		}
	}
	
	return value;
}

void pic4_Image_set(pic4_Image* image, float x, float y, uint8_t value) {
	*(image->buffer + pic4_calcIndexByPosition(image->level, image->precision, x, y)) = value;
}

void pic4_Image_read(pic4_Image* image, const char* filename) {
	FILE* f = fopen(filename, "r");

	if (f != NULL) {
		fseek(f, 0, SEEK_END);

		const size_t bufferSize = (size_t) ftell(f);

		fseek(f, 0, SEEK_SET);

		image->level = pic4_calcLevelByBufferSize(bufferSize);
		image->precision = pic4_calcPrecision(image->level);
		image->buffer = (uint8_t*) malloc(sizeof(uint8_t) * bufferSize);

		fread((void*) image->buffer, sizeof(uint8_t), bufferSize, f);

		fclose(f);
	}
}

void pic4_Image_write(const pic4_Image* image, const char* filename) {
	FILE* f = fopen(filename, "w");

	if (f != NULL) {
		const size_t bufferSize = pic4_calcBufferSizeByLevel(image->level);

		fseek(f, 0, SEEK_SET);

		fwrite((void*) image->buffer, sizeof(uint8_t), bufferSize, f);

		fflush(f);

		fclose(f);
	}
}
