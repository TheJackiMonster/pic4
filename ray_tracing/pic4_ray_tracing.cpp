//
// Created by thejackimonster on 05.07.18.
//

#include <pic4.hpp>
#include <vector>
#include <math.h>
#include <stdlib.h>

struct Camera {
	float px, py, pz;
	float dx, dy, dz;
	float ux, uy, uz;
	float sx, sy, sz;
};

struct Sphere {
	float px, py, pz;
	float r;
	float dc, ec;
};

struct Ray {
	float px, py, pz;
	float dx, dy, dz;
};

inline void normalize(float& x, float& y, float& z) {
	float l = sqrtf(x*x + y*y + z*z);

	if (l > 0.0f) {
		x /= l;
		y /= l;
		z /= l;
	}
}

bool trace_hit(const Ray& ray, const Sphere& s, float& min, float& max) {
	float dx = s.px - ray.px;
	float dy = s.py - ray.py;
	float dz = s.pz - ray.pz;

	float q = dx * ray.dx + dy * ray.dy + dz * ray.dz;

	if (q < 0.0f) {
		return false;
	}

	float sx = dy * ray.dz - dz * ray.dy;
	float sy = dz * ray.dx - dx * ray.dz;
	float sz = dx * ray.dy - dy * ray.dx;

	float nx = sz * ray.dy - sy * ray.dz;
	float ny = sx * ray.dz - sz * ray.dx;
	float nz = sy * ray.dx - sx * ray.dy;

	normalize(nx, ny, nz);

	float d = dx * nx + dy * ny + dz * nz;

	if (d > s.r) {
		return false;
	}

	float hx = s.px + nx * d - ray.px;
	float hy = s.py + ny * d - ray.py;
	float hz = s.pz + nz * d - ray.pz;

	float hit = hx * ray.dx + hy * ray.dy + hz * ray.dz;

	min = hit - d;
	max = hit + d;

	if (max < min) {
		return false;
	}

	return (min > 0.0f);
}

float trace_ray(const std::vector<Sphere>& spheres, const Ray& ray, int magic, bool diffuse = true) {
	static float min, max;

	float val = 0.0f;

	const Sphere* s_hit = nullptr;
	float t_hit = MAXFLOAT;

	for (const Sphere& s : spheres) {
		if (trace_hit(ray, s, min, max)) {
			if (min < t_hit) {
				s_hit = &s;
				t_hit = min;
			}
		}
	}

	if (s_hit != nullptr) {
		float ec = s_hit->ec;

		const float ec_mode = (1.0f - s_hit->ec) * (1.0f - s_hit->ec);

		if (magic > 0) {
			Ray ray0 = ray;

			ray0.px = ray.px + ray.dx * t_hit;
			ray0.py = ray.py + ray.dy * t_hit;
			ray0.pz = ray.pz + ray.dz * t_hit;

			float nx = (ray0.px - s_hit->px) / s_hit->r;
			float ny = (ray0.py - s_hit->py) / s_hit->r;
			float nz = (ray0.pz - s_hit->pz) / s_hit->r;

			float nrs;

			for (const Sphere& s : spheres) {
				ray0.dx = s.px - ray0.px;
				ray0.dy = s.py - ray0.py;
				ray0.dz = s.pz - ray0.pz;

				normalize(ray0.dx, ray0.dy, ray0.dz);

				nrs = nx * ray0.dx + ny * ray0.dy + nz * ray0.dz;

				if (nrs > 0.0f) {
					ec += trace_ray(spheres, ray0, magic - 1, true) * nrs;
				}
			}
		}

		if (diffuse) {
			val += ec * s_hit->dc;
		} else {
			val += ec;
		}
	}

	if ((diffuse) && (val > 1.0f)) {
		return 1.0f;
	} else {
		return val;
	}
}

int main(int argc, char** argv) {
	pic4::Image img (12);

	Camera cam {
		0, 0, 0,
		0, 0, 1,
		0, 1, 0,
		1, 0, 0
	};

	std::vector<Sphere> spheres;

	spheres.push_back({ 1.0f, 0.5f, 3.0f, 0.6f, 0.65f, 0.8f });
	spheres.push_back({ -0.4f, -0.7f, 3.5f, 0.2f, 0.4f, 0.0f });
	spheres.push_back({ -0.4f, +0.0f, 3.5f, 0.2f, 0.4f, 0.1f });
	spheres.push_back({ -0.4f, +0.7f, 3.5f, 0.2f, 0.4f, 0.7f });

	Ray ray;
	float val;
	uint8_t col;

	for (pic4::Point& p : img) {
		ray.px = cam.px;
		ray.py = cam.py;
		ray.pz = cam.pz;

		ray.dx = cam.dx + (p.x() - 0.5f) * cam.sx + (p.y() - 0.5f) * cam.ux;
		ray.dy = cam.dy + (p.x() - 0.5f) * cam.sy + (p.y() - 0.5f) * cam.uy;
		ray.dz = cam.dz + (p.x() - 0.5f) * cam.sz + (p.y() - 0.5f) * cam.uz;

		normalize(ray.dx, ray.dy, ray.dz);

		val = trace_ray(spheres, ray, 8);
		col = (uint8_t) (val * 0xFF);

		p.set(col);
	}

	img.write("ray_test.p4");

	return 0;
}
