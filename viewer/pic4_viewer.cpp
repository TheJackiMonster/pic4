//
// Created by thejackimonster on 19.11.17.
//

#include <opencv2/core.hpp>
#include <opencv2/imgproc.hpp>
#include <opencv2/highgui.hpp>

#include <pic4.hpp>

void encode(pic4::Image& img) {
	size_t N = pic4_calcBufferSizeByLevel(img.level);
	float fx, fy;
	uint8_t l;
	size_t j;

	for (size_t i = 1; i < N; i++) {
		l = pic4_calcLevelByBufferSize(i);
		pic4_calcPositionByIndex(i, &fx, &fy);
		j = pic4_calcIndexByPosition(l, pic4_calcPrecision(l), fx, fy);
		img.buffer[i] += img.buffer[j];
	}
}

#define SQ(x) (x)*(x)
#define SQRT(x) static_cast<uint32_t>(sqrt(static_cast<double>(x)))

template <int mode>
uint8_t getValueByMode(const pic4::Image* img, float x, float y) {
	if (img == nullptr) {
		return 128;
	}

	const float p = (img->precision / 4);

	uint32_t v [9];

	v[0] = img->get(x, y);

	if ((mode >= 1) && (mode <= 7)) {
		v[1] = img->get(x - p, y);
		v[2] = img->get(x + p, y);
		v[3] = img->get(x, y - p);
		v[4] = img->get(x, y + p);

		v[5] = img->get(x - p, y - p);
		v[6] = img->get(x + p, y - p);
		v[7] = img->get(x + p, y + p);
		v[8] = img->get(x - p, y + p);
	}

	uint32_t value = 0;
	int k;

	switch (mode) {
		case 1: {
			for (k = 0; k < 9; k++) {
				value += v[k];
			}

			value /= 9;
			break; }
		case 2: {
			value += 4 * v[0];

			for (k = 0; k < 4; k++) {
				value += 2 * v[1 + k];
				value += v[5 + k];
			}

			value /= 16;
			break; }
		case 3: {
			value = v[0];

			for (k = 1; k < 9; k++) {
				value = MIN(value, v[k]);
			}

			break; }
		case 4: {
			value = v[0];

			for (k = 1; k < 9; k++) {
				value = MAX(value, v[k]);
			}

			break; }
		case 5: {
			value = SQRT(SQ(v[5] - v[7]) + SQ(v[6] - v[8]));
			break; }
		case 6: {
			uint32_t min = v[0];
			uint32_t max = v[0];

			for (k = 1; k < 9; k++) {
				min = MIN(min, v[k]);
				max = MAX(max, v[k]);
			}

			value = (max - min);

			break; }
		case 7: {
			value += SQRT(2 * SQ(v[3] - v[4]) + SQ(v[5] - v[8]) + SQ(v[6] - v[7]));
			value += SQRT(2 * SQ(v[1] - v[2]) + SQ(v[5] - v[6]) + SQ(v[7] - v[8]));

			value /= 2;
			break; }
		default:
			value = v[0];
			break;
	}

	return static_cast<uint8_t>(MIN(value, 0xFF));
}

typedef void (*pic4_render_func)(cv::Mat& image, int imageSize, const pic4::Image* imgY, const pic4::Image* imgU, const pic4::Image* imgV);

template <int mode>
void renderImage(cv::Mat& image, int imageSize, const pic4::Image* imgY, const pic4::Image* imgU, const pic4::Image* imgV) {
	int i, j, k;

#pragma omp parallel for
	for (i = 0; i < imageSize; i++) {
		const float x = (0.5f + i) / imageSize;
#pragma omp parallel for
		for (j = 0; j < imageSize; j++) {
			const float y = (0.5f + j) / imageSize;

			auto& px = image.at<cv::Vec3b>(cv::Point(i, j));

			uint8_t Y = getValueByMode<mode>(imgY, x, y);
			uint8_t U = getValueByMode<mode>(imgU, x, y);
			uint8_t V = getValueByMode<mode>(imgV, x, y);

			switch (mode) {
				case 5:
					px[0] = Y;
					px[1] = U;
					px[2] = V;
					break;
				case 6:
					px[0] = Y;
					px[1] = U;
					px[2] = V;
					break;
				case 7:
					px[0] = Y;
					px[1] = U;
					px[2] = V;
					break;
				default:
					float u = (U - 128.0f);
					float v = (V - 128.0f);

					float c0 = Y - 1.21889e-6f * u + 1.402f * v;
					float c1 = Y - 0.344136f * u - 0.714136f * v;
					float c2 = Y + 1.772f * u + 4.06298e-7f * v;

					px[0] = static_cast<uint8_t>(MIN(MAX(c0, 0), 0xFF));
					px[1] = static_cast<uint8_t>(MIN(MAX(c1, 0), 0xFF));
					px[2] = static_cast<uint8_t>(MIN(MAX(c2, 0), 0xFF));
					break;
			}
		}
	}
}

int main(int argc, char** argv) {
	if (argc <= 1) {
		return 1;
	}

	const char* win_name = "pic4_viewer\0";

	char title [256];

	sprintf(title, "PIC4: ( %s )\0", argv[1]);

	cv::namedWindow(win_name, cv::WINDOW_KEEPRATIO);
	cv::setWindowTitle(win_name, title);

	pic4::Image* origin [3] = { nullptr, nullptr, nullptr };

	origin[0] = new pic4::Image(argv[1]);

	uint8_t lvl [3] = { origin[0]->level, 0, 0 };

	if (argc > 2) {
		uint8_t arg_lvl = static_cast<uint8_t>(atoi(argv[2]));

		if (arg_lvl > 0) {
			lvl[0] = arg_lvl;
			lvl[1] = arg_lvl;
			lvl[2] = arg_lvl;
		}
	}

	uint8_t layer = 0;
	uint8_t current_lvl = lvl[layer];

	int imgSize = 1 << (current_lvl - 1);

	if (argc > 3) {
		imgSize = atoi(argv[3]);
	}

	if (imgSize < 64) {
		imgSize = 64;
	}

	if (imgSize > 1024) {
		imgSize = 1024;
	}

	if (argc > 4) {
		origin[1] = new pic4::Image(argv[4]);

		if (lvl[1] == 0) {
			lvl[1] = origin[1]->level;
		}
	}

	if (argc > 5) {
		origin[2] = new pic4::Image(argv[5]);

		if (lvl[2] == 0) {
			lvl[2] = origin[2]->level;
		}
	}

	cv::Mat image = cv::Mat::zeros(imgSize, imgSize, CV_8UC3);

	pic4::Image* img [3] = { nullptr, nullptr, nullptr };

	for (uint8_t i = 0; i < 3; i++) {
		if (origin[i] != nullptr) {
			encode(*(origin[i]));

			img[i] = new pic4::Image((*(origin[i]))[lvl[i]]);
		}
	}

	int key;
	bool exit = false;

	pic4_render_func basic_render = renderImage<0>;

	do {
		if (lvl[layer] != current_lvl) {
			if (img[layer] != nullptr) {
				delete img[layer];
			}

			for (uint8_t i = 0; i < 3; i++) {
				uint8_t corrected_lvl = MIN(lvl[0], lvl[i]);

				if (origin[i] != nullptr) {
					img[i] = new pic4::Image((*(origin[i]))[corrected_lvl]);
				}
			}
		}

		basic_render(image, imgSize, img[0], img[1], img[2]);

		unsigned long size = pic4_calcBufferSizeByLevel(lvl[0]);

		if (img[1] != nullptr) {
			size += pic4_calcBufferSizeByLevel(lvl[1]);
		}

		if (img[2] != nullptr) {
			size += pic4_calcBufferSizeByLevel(lvl[2]);
		}

		sprintf(title, "PIC4: ( %s ) - { %u %u %u } = %lu\0", argv[1], lvl[0], lvl[1], lvl[2], size);

		cv::setWindowTitle(win_name, title);
		cv::imshow(win_name, image);

		key = cv::waitKey(0);

		current_lvl = lvl[layer];

		switch (key) {
			case '0':
				basic_render = renderImage<0>;
				break;
			case '1':
				basic_render = renderImage<1>;
				break;
			case '2':
				basic_render = renderImage<2>;
				break;
			case '3':
				basic_render = renderImage<3>;
				break;
			case '4':
				basic_render = renderImage<4>;
				break;
			case '5':
				basic_render = renderImage<5>;
				break;
			case '6':
				basic_render = renderImage<6>;
				break;
			case '7':
				basic_render = renderImage<7>;
				break;
			case '8':
				basic_render = renderImage<8>;
				break;
			case '9':
				basic_render = renderImage<9>;
				break;
			case '+':
				if (lvl[layer] < origin[layer]->level) {
					lvl[layer]++;
				}

				break;
			case '-':
				if (lvl[layer] > 1) {
					lvl[layer]--;
				}

				break;
			case 'y':
				if (origin[0] != nullptr) {
					layer = 0;
				}

				break;
			case 'u':
				if (origin[1] != nullptr) {
					layer = 1;
				}

				break;
			case 'v':
				if (origin[2] != nullptr) {
					layer = 2;
				}

				break;
			default:
				exit = true;
				break;
		}
	} while (!exit);

	for (uint8_t i = 0; i < 3; i++) {
		if (img[i] != nullptr) {
			delete img[i];
		}

		if (origin[i] != nullptr) {
			delete origin[i];
		}
	}

	return 0;
}
