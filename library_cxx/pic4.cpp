//
// Created by thejackimonster on 19.11.17.
//

#include "pic4.hpp"

#include <algorithm>
#include <cstdio>

namespace pic4 {

	float Point::x() const {
		return this->px;
	}

	float Point::y() const {
		return this->py;
	}

	uint8_t Point::get() const {
		return *(this->pbuf + this->pindex);
	}

	void Point::set(uint8_t value) {
		*(this->pbuf + this->pindex) = value;
	}

	float ConstPoint::x() const {
		return this->px;
	}

	float ConstPoint::y() const {
		return this->py;
	}

	uint8_t ConstPoint::get() const {
		return *(this->pbuf + this->pindex);
	}

	Image::Iterator::Iterator(uint8_t* buffer, size_t index) {
		this->point.pbuf = buffer;
		this->point.pindex = index;

		pic4_calcPositionByIndex(this->point.pindex, &(this->point.px), &(this->point.py));
	}

	Image::Iterator Image::Iterator::operator++() {
		Iterator iterator = *this;
		pic4_calcPositionByIndex(++(this->point.pindex), &(this->point.px), &(this->point.py));
		return iterator;
	}

	Image::Iterator Image::Iterator::operator++(int junk) {
		pic4_calcPositionByIndex(++(this->point.pindex), &(this->point.px), &(this->point.py));
		return *this;
	}

	Point& Image::Iterator::operator*() {
		return this->point;
	}

	Point* Image::Iterator::operator->() {
		return &(this->point);
	}

	bool Image::Iterator::operator==(const Iterator& other) {
		return this->point.pindex == other.point.pindex;
	}

	bool Image::Iterator::operator!=(const Iterator& other) {
		return this->point.pindex != other.point.pindex;
	}

	Image::ConstIterator::ConstIterator(const uint8_t* buffer, size_t index) {
		this->point.pbuf = buffer;
		this->point.pindex = index;

		pic4_calcPositionByIndex(this->point.pindex, &(this->point.px), &(this->point.py));
	}

	Image::ConstIterator Image::ConstIterator::operator++() {
		ConstIterator iterator = *this;
		pic4_calcPositionByIndex(++(this->point.pindex), &(this->point.px), &(this->point.py));
		return iterator;
	}

	Image::ConstIterator Image::ConstIterator::operator++(int junk) {
		pic4_calcPositionByIndex(++(this->point.pindex), &(this->point.px), &(this->point.py));
		return *this;
	}

	const ConstPoint& Image::ConstIterator::operator*() {
		return this->point;
	}

	const ConstPoint* Image::ConstIterator::operator->() {
		return &(this->point);
	}

	bool Image::ConstIterator::operator==(const ConstIterator& other) {
		return this->point.pindex == other.point.pindex;
	}

	bool Image::ConstIterator::operator!=(const ConstIterator& other) {
		return this->point.pindex != other.point.pindex;
	}

	Image::Image(uint8_t level) {
		this->level = level;
		this->precision = pic4_calcPrecision(level);
		this->bufferSize = pic4_calcBufferSizeByLevel(this->level);
		this->buffer = new uint8_t[ this->bufferSize ];
	}

	Image::Image(const char* filename) {
		FILE* f = fopen(filename, "r");

		fseek(f, 0, SEEK_END);

		this->bufferSize = static_cast<size_t>(ftell(f));

		fseek(f, 0, SEEK_SET);

		this->level = pic4_calcLevelByBufferSize(this->bufferSize);
		this->precision = pic4_calcPrecision(this->level);
		this->buffer = new uint8_t[ this->bufferSize ];

		fread((void*) this->buffer, sizeof(uint8_t), this->bufferSize, f);

		fclose(f);
	}

	Image::~Image() {
		delete[] this->buffer;
	}

	Image::Image(const Image& other) {
		this->level = other.level;
		this->precision = other.precision;
		this->bufferSize = other.bufferSize;
		this->buffer = new uint8_t[ this->bufferSize ];

		std::copy(other.buffer, other.buffer + this->bufferSize, this->buffer);
	}

	Image::Image(Image&& other) {
		this->level = other.level;
		this->precision = other.precision;
		this->bufferSize = other.bufferSize;
		this->buffer = other.buffer;

		other.buffer = nullptr;
	}

	Image& Image::operator=(const Image& other) {
		this->bufferSize = other.bufferSize;

		if (this->level < other.level) {
			delete[] this->buffer;

			this->buffer = new uint8_t[ this->bufferSize ];
		}

		this->level = other.level;
		this->precision = other.precision;

		std::copy(other.buffer, other.buffer + this->bufferSize, this->buffer);

		return *this;
	}

	Image& Image::operator=(Image&& other) {
		delete[] this->buffer;

		this->level = other.level;
		this->precision = other.precision;
		this->bufferSize = other.bufferSize;
		this->buffer = other.buffer;

		other.buffer = nullptr;

		return *this;
	}

	Image Image::operator[](uint8_t level) const {
		Image sub (level == 0 || level > this->level? this->level : level);

		std::copy(this->buffer, this->buffer + sub.bufferSize, sub.buffer);

		return sub;
	}

	const uint8_t& Image::operator()(float x, float y) const {
		return *(this->buffer + pic4_calcIndexByPosition(this->level, this->precision, x, y));
	}

	uint8_t& Image::operator()(float x, float y) {
		return *(this->buffer + pic4_calcIndexByPosition(this->level, this->precision, x, y));
	}

	uint8_t Image::get(float x, float y) const {
		return pic4_Image_get(this, x, y);
	}

	void Image::set(float x, float y, uint8_t value) {
		pic4_Image_set(this, x, y, value);
	}

	void Image::write(const char* filename) const {
		FILE* f = fopen(filename, "w");

		fseek(f, 0, SEEK_SET);

		fwrite((void*) this->buffer, sizeof(uint8_t), bufferSize, f);

		fflush(f);

		fclose(f);
	}

	Image::ConstIterator Image::begin() const {
		return ConstIterator(this->buffer, 0);
	}

	Image::Iterator Image::begin() {
		return Iterator(this->buffer, 0);
	}

	Image::ConstIterator Image::end() const {
		return ConstIterator(this->buffer, this->bufferSize);
	}

	Image::Iterator Image::end() {
		return Iterator(this->buffer, this->bufferSize);
	}

}
