#pragma once

//
// Created by thejackimonster on 19.11.17.
//

#ifndef PIC4_HPP_
#define PIC4_HPP_

#include <pic4.h>

namespace pic4 {

	struct Point {
		friend class Image;

	private:
		uint8_t* pbuf;
		size_t pindex;

		float px;
		float py;

	public:
		float x() const;

		float y() const;

		uint8_t get() const;

		void set(uint8_t value);

	};

	struct ConstPoint {
		friend class Image;

	private:
		const uint8_t* pbuf;
		size_t pindex;

		float px;
		float py;

	public:
		float x() const;

		float y() const;

		uint8_t get() const;

	};

	class Image : public pic4_Image {
	private:
		size_t bufferSize;

	public:

		class Iterator {
		private:
			Point point;

		public:
			Iterator(uint8_t* buffer, size_t index);

			Iterator operator++();

			Iterator operator++(int junk);

			Point& operator*();

			Point* operator->();

			bool operator==(const Iterator& other);

			bool operator!=(const Iterator& other);

		};

		class ConstIterator {
		private:
			ConstPoint point;

		public:
			ConstIterator(const uint8_t* buffer, size_t index);

			ConstIterator operator++();

			ConstIterator operator++(int junk);

			const ConstPoint& operator*();

			const ConstPoint* operator->();

			bool operator==(const ConstIterator& other);

			bool operator!=(const ConstIterator& other);

		};

		Image(uint8_t level);

		Image(const char* filename);

		~Image();

		Image(const Image& other);

		Image(Image&& other);

		Image& operator=(const Image& other);

		Image& operator=(Image&& other);

		Image operator[](uint8_t level) const;

		const uint8_t& operator()(float x, float y) const;

		uint8_t& operator()(float x, float y);

		uint8_t get(float x, float y) const;

		void set(float x, float y, uint8_t value);

		void write(const char* filename) const;

		ConstIterator begin() const;

		Iterator begin();

		ConstIterator end() const;

		Iterator end();

	};

}

#endif /* PIC4_HPP_ */