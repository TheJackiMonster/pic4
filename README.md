# pic4
A library for a raw-image-format which doesn't use any headers at all.


![](layers.gif)

## Idea

The format uses a variable amount of layers which scale up by the factor of two in each dimension. 

### Advantages

- No space needed for special options etc. in headers. (actually no header at all)
- Loading pixels from file and visualizing them can be implemented parallel because the resolution will just scale up dynamicly by loading more pixels.
- The pixels in the picture can be iterated linear easily and be treated as one-dimensional array.
- Scaling the picture is not necessary because you can manage how much detail should be used for visualizing.
- The files can be compressed of course and can even become smaller than other image-file-formats.

### Disadvantages

- Every picture will be treated as square.
- No implicit compression, which means higher size of files.

## Contents

### Libraries

The repository contains a simple and short library written in C. But if you like a little more comfort, you can use the library written in C++ using the functions written in C. So the functionality isn't any different.

### Programs

The repository contains short programs to use the new image-format locally without writing own programs on startup. For example there is a simple program to view the image-contents and another program to convert image-files with a familiar format to this format.
